############################
# STEP 1 build executable binary
############################
FROM golang:1.13-alpine as skicka-builder

# Add Maintainer Info
LABEL maintainer="Thomas RATOUIT <thoratou@gmail.com>"

# Install git + SSL ca certificates.
# Git is required for fetching the dependencies.
# Ca-certificates is required to call HTTPS endpoints.
RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates

RUN CGO_ENABLED=0 go get -a -ldflags '-s' github.com/google/skicka

############################
# STEP 2 build a small image
############################
FROM alpine:latest

# Add Maintainer Info
LABEL maintainer="Thomas RATOUIT <thoratou@gmail.com>"

# Add inotify
RUN apk --no-cache add inotify-tools

# Copy our static executable
COPY --from=skicka-builder /go/bin/skicka /usr/bin/skicka

# su-exec
RUN apk --no-cache add su-exec
RUN chmod u+s /sbin/su-exec

# Bash
RUN apk --no-cache add bash

#Home dir
WORKDIR /home/appuser_skicka

# Copy script
RUN mkdir /home/appuser
COPY backup.sh /home/appuser/backup.sh
COPY docker-entrypoint.sh /home/appuser/docker-entrypoint.sh

ENTRYPOINT ["sh", "/home/appuser/docker-entrypoint.sh"]

CMD bash /home/appuser/backup.sh