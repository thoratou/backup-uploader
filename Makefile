REPO_ROOT = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
USER = $(shell echo $$USER)

.PHONY: all
all: build run

.PHONY: build
build:
	@printf "\033[32m Local build\n\033[0m"
	@docker build -t backup-uploader .

.PHONY: init
init: create-local-folders
	@printf "\033[32m Init skicka\n\033[0m"
	@docker run -it --rm \
		--name backup-uploader-test \
		-v $(REPO_ROOT)/skicka:/home/appuser_skicka \
		-v $(REPO_ROOT)/test.backup:/home/appuser/data \
		backup-uploader su-exec appuser skicka init
	@docker run -it --rm \
		--name backup-uploader-test \
		-v $(REPO_ROOT)/skicka:/home/appuser_skicka \
		-v $(REPO_ROOT)/test.backup:/home/appuser/data \
		backup-uploader su-exec appuser skicka -no-browser-auth ls

.PHONY: ls
ls: create-local-folders
	@printf "\033[32m Display drive root content\n\033[0m"
	@docker run -it --rm \
		--name backup-uploader-test \
		-v $(REPO_ROOT)/skicka:/home/appuser_skicka \
		-v $(REPO_ROOT)/test.backup:/home/appuser/data \
		backup-uploader su-exec appuser skicka ls

.PHONY: run
run: create-local-folders
	@printf "\033[32m Run local\n\033[0m"
	@docker run -it --rm \
		--name backup-uploader-test \
		-e MAX_BACKUP_NUMBER=2 \
		-e DRIVE_PATH=test.backup \
		-v $(REPO_ROOT)/skicka:/home/appuser_skicka \
		-v $(REPO_ROOT)/test.backup:/home/appuser/data \
		backup-uploader

.PHONY: bash
bash:
	@printf "\033[32m Open bash on existing container\n\033[0m"
	@docker exec -it backup-uploader-test su-exec appuser bash


.PHONY: create-local-folders
create-local-folders:
	@mkdir -p $(REPO_ROOT)/skicka
	@mkdir -p $(REPO_ROOT)/test.backup
