#!/bin/bash

#Are we ok ?
if [ -z "$DRIVE_PATH" ]; then echo "Missing DRIVE_PATH env variable" && exit 1; fi
if [ -z "$MAX_BACKUP_NUMBER" ]; then echo "Missing MAX_BACKUP_NUMBER env variable" && exit 1; fi

uploadNewFolders()
{
  cd /home/appuser/data
  SKICKA_FOLDERS=($(skicka ls /$DRIVE_PATH | grep "/" | awk -F'/' '{print $1}'))
  LOCAL_FOLDERS=($(ls -d */ | awk -F'/' '{print $1}'))
  for FOLDER in "${LOCAL_FOLDERS[@]}"
  do
    if [[ " ${SKICKA_FOLDERS[@]} " =~ " ${FOLDER}" ]]; then
      echo "folder $FOLDER already uploaded, skip it"
    else
      echo "folder $FOLDER to be uploaded"
      skicka upload $FOLDER /$DRIVE_PATH/$FOLDER
      echo "folder $FOLDER uploaded"
    fi
  done
}


cleanUpFolders()
{
  FOLDERS=($(skicka ls /$DRIVE_PATH))
  NUM_FOLDERS=${#FOLDERS[@]}
  echo "number of folders: $NUM_FOLDERS"
  if [ "$NUM_FOLDERS" -gt "$MAX_BACKUP_NUMBER" ]; then
    NUM_FOLDERS_TO_REMOVE=$(expr $NUM_FOLDERS - $MAX_BACKUP_NUMBER)
    echo "number of folders to remove: $NUM_FOLDERS_TO_REMOVE"
    for i in $(seq 0 $(expr $NUM_FOLDERS_TO_REMOVE - 1));
    do
      TO_REMOVE=${FOLDERS[$i]}
      echo "removing folder: $TO_REMOVE"
      skicka rm -s -r /$DRIVE_PATH/$TO_REMOVE
      rm -rf /home/appuser/data/$TO_REMOVE
      echo "folder $TO_REMOVE removed"
    done
  fi
}

backupwait()
{
  skicka mkdir -p /$DRIVE_PATH
  while inotifywait -q -e create,delete,modify,move /home/appuser/data >/dev/null; do
    echo "Backup change detected"
    uploadNewFolders
    cleanUpFolders
  done
}

cleanup()
{
  echo "Signal catched, stopping container"
  echo "Container stopped"
  exit 0
}

trap cleanup SIGTERM

echo "Backup Uploader started"
backupwait &
while :; do
    sleep 1s
done
