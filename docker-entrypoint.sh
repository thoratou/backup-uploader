#!/bin/bash

if [ ! -d "/home/appuser/data" ]; then
  echo "Missing /home/appuser/data folder, please add a volue mount"
  exit 1
fi

export USER=appuser
export UID=$(stat -c "%u" /home/appuser/data)
export GID=$(stat -c "%u" /home/appuser/data)

echo "Adding host user and group"

GROUP_LINE=$(grep ":$GID:" /etc/group)
if [ -z "$GROUP_LINE" ]; then
    #no group, create it
        su-exec root addgroup --gid ${GID} ${USER}
        su-exec root adduser \
		--disabled-password \
		--gecos "" \
		--home "/home/appuser_skicka" \
		--ingroup "$USER" \
		--uid "$UID" \
		"$USER"
    else
        #existing group, retrieve group name and add to group
        GROUP=$(echo "$GROUP_LINE" | awk -F':' '{print $1}')
        su-exec root adduser \
                --disabled-password \
                --gecos "" \
                --home "/home/appuser_skicka" \
                --ingroup "$GROUP" \
                --uid "$UID" \
                "$USER"
    fi

su-exec root chown -R appuser:appuser /home/appuser
su-exec root chown -R appuser:appuser /home/appuser_skicka

#many thanks https://blog.codeship.com/trapping-signals-in-docker-containers/

pid=0

# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  exit 143; # 128 + 15 -- SIGTERM
}

trap 'kill ${!}; term_handler' SIGTERM

su-exec appuser "$@" &
pid="$!"

# wait indefinitely
while true
do
  tail -f /dev/null & wait ${!}
done
